/*
 * supape.h
 *
 * Created: 6/7/2015 5:39:48 PM
 *  Author: Sergiu
 */ 


#ifndef SUPAPE_H_
#define SUPAPE_H_

#include <avr/io.h>


#define SUPAPE_DDR DDRD
#define SUPAPE_Port PORTD
#define SUPAPE_Pin PIND5


void initSupape(void);

void TurnOnSupape(void);

void TurnOffSupape(void);

#endif /* SUPAPE_H_ */