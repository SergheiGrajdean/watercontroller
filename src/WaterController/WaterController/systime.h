/*
 * systime.h
 *
 * Created: 20.04.2015 21:29:59
 *  Author: Home
 */ 


#ifndef SYSTIME_H_
#define SYSTIME_H_

void initSystime();

unsigned long millis();

#endif /* SYSTIME_H_ */