/*
 * supape.c
 *
 * Created: 6/7/2015 5:42:18 PM
 *  Author: Sergiu
 */ 

#include "supape.h"

void initSupape(void)
{
	SUPAPE_DDR |= (1<<SUPAPE_Pin);
}

void TurnOnSupape(void)
{
	SUPAPE_Port |= (1<<SUPAPE_Pin);
}

void TurnOffSupape(void)
{
	SUPAPE_Port &= ~(1<<SUPAPE_Pin);
}