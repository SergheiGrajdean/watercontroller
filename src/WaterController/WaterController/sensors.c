/*
 * sensors.c
 *
 * Created: 28.10.2014 8:28:20
 *  Author: Home
 */ 

#include "sensors.h"
#include <stdlib.h>

#define Is_At_Level(PORT_IN, PIN) ((PORT_IN & 1<<PIN) == (1<<PIN))

#define Is_Low_Level Is_At_Level(LowLevelSensor_IN, LowLevelSensor_Pin)
#define Is_Medium_Level Is_At_Level(MediumLevelSensor_IN, MediumLevelSensor_Pin)
#define Is_Full_Level Is_At_Level(FullLevelSensor_IN, FullLevelSensor_Pin)
#define Is_Overflow_First_Level Is_At_Level(OverflowFirstLevelSensor_IN, OverflowFirstLevelSensor_Pin)
#define Is_Overflow_Second_Level Is_At_Level(OverflowSecondLevelSensor_IN, OverflowSecondLevelSensor_Pin)

Sensor _lowLevelSensor;
Sensor _mediumLevelSensor;
Sensor _highLevelSensor;
Sensor _overflowFirstLevelSensor;
Sensor _overflowSecondLevelSensor;

char isLowLevel()
{
	return Is_Low_Level;
}

char isMediumLevel()
{
	return Is_Medium_Level;
}

char isFullLevel()
{
	return Is_Full_Level;
}

char isOverflowFirstLevel()
{
	return Is_Overflow_First_Level;
}

char isOverflowSecondLevel()
{
	return Is_Overflow_Second_Level;
}

void initSensors()
{
	LowLevelSensor_DDR &= ~(1<<LowLevelSensor_Pin);
	MediumLevelSensor_DDR &= ~(1<<MediumLevelSensor_Pin);
	FullLevelSensor_DDR &= ~(1<<FullLevelSensor_Pin);
	OverflowFirstLevelSensor_DDR &= ~(1<<OverflowFirstLevelSensor_Pin);
	OverflowSecondLevelSensor_DDR &= ~(1<<OverflowSecondLevelSensor_Pin);
	
	unsigned int setTimeDelta = 2000;
	
	initSensor(&_lowLevelSensor, &isLowLevel, setTimeDelta);
	initSensor(&_mediumLevelSensor, &isMediumLevel, setTimeDelta);
	initSensor(&_highLevelSensor, &isFullLevel, setTimeDelta);
	initSensor(&_overflowFirstLevelSensor, &isOverflowFirstLevel, setTimeDelta);
	initSensor(&_overflowSecondLevelSensor, &isOverflowSecondLevel, setTimeDelta);
}

WaterLevel getWaterLevel()
{
	char status = 0;
	
	if(_lowLevelSensor.ContactDetected == Detected)
	{
		status |= (1<<0);
	}
	
	if(_mediumLevelSensor.ContactDetected == Detected)
	{
		status |= (1<<1);
	}
	
	if(_highLevelSensor.ContactDetected == Detected)
	{
		status |= (1<<2);
	}
	if(_overflowFirstLevelSensor.ContactDetected == Detected)
	{
		status |= (1<<3);
	}
	
	if(_overflowSecondLevelSensor.ContactDetected == Detected)
	{
		status |= (1<<4);
	}
	
	switch(status)
	{
		case 0x00 : 
		case 0x01 : return Low;
		case 0x03 : return Medium;
		case 0x07 : return Full;
		case 0x0F :
		case 0x1F : return OverFull;
		default : return Error;
		
	}
}

void updateSensors(unsigned long currentTime)
{
	updateSensor(&_lowLevelSensor, currentTime);
	updateSensor(&_mediumLevelSensor, currentTime);
	updateSensor(&_highLevelSensor, currentTime);
	updateSensor(&_overflowFirstLevelSensor, currentTime);
	updateSensor(&_overflowSecondLevelSensor, currentTime);
}

void updateSensor(Sensor* sensor, unsigned long currentTime)
{
	char isContact = sensor->IsContactDetected();
		
	if(isContact)
	{
		if(sensor->ContactDetected == Detected)
		{
			return;
		}
		
		if(sensor->IsChecked == Unchecked)
		{
			sensor->IsChecked = Checked;
			sensor->checkTime = currentTime;
		}
		else if(currentTime - sensor->checkTime > sensor->setTimeDelta)
		{
			sensor->ContactDetected = Detected;
		}
	}
	else
	{
		if(sensor->ContactDetected == Undetected)
		{
			return;
		}
		
		if(sensor->IsChecked == Checked)
		{
			sensor->IsChecked = Unchecked;
			sensor->checkTime = currentTime;
		}
		else if(currentTime - sensor->checkTime > sensor->setTimeDelta)
		{
			sensor->ContactDetected = Undetected;
		}
	}
}

void initSensor(Sensor* sensor,  contactDetectorPtr contactDetector, unsigned int setTimeDelta)
{
	sensor->checkTime =0;
	sensor->ContactDetected = Undetected;
	sensor->IsChecked = Unchecked;
	sensor->IsContactDetected = contactDetector;
	sensor->setTimeDelta = setTimeDelta;
}
