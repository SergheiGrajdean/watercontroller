/*
 * button.h
 *
 * Created: 27.10.2014 16:28:42
 *  Author: Serghei Grajdean
 */ 

#ifndef BUTTON_H_
#define BUTTON_H_

#include <avr/io.h>
#include <stdbool.h>
#include "systime.h"

enum KeyState
{
	KeyUp,
	KeyDown	
};

typedef void (*clickHandlerPtr)();
typedef void (*longClickHandlerPtr)();

typedef struct ButtonType
{
	unsigned long pressTime;
	enum KeyState keyState;
	clickHandlerPtr clickHandler;
	longClickHandlerPtr longClickHandler;
} Button;

void updateButton(bool isPressed, Button* buttonPtr);

Button* createButton();

#endif /* BUTTON_H_ */