/*
 * systime.c
 *
 * Created: 20.04.2015 21:30:16
 *  Author: Home
 */ 

#include "systime.h"

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#define clockCyclesPerMicrosecond() ( F_CPU / 1000000L )
#define PRESCALER_FACTOR 8
#define CLOCK_OVERFLOW 128
#define clockCyclesToMicroseconds(a) ( ((a) * 1000L) / (F_CPU / 1000L) )
#define MICROSECONDS_PER_TIMER0_OVERFLOW (clockCyclesToMicroseconds(PRESCALER_FACTOR * CLOCK_OVERFLOW))
// the whole number of milliseconds per timer0 overflow
#define MILLIS_INC (MICROSECONDS_PER_TIMER0_OVERFLOW / 1000)
// the fractional number of milliseconds per timer0 overflow. we shift right
// by three to fit these numbers into a byte. (for the clock speeds we care
// about - 8 and 16 MHz - this doesn't lose precision.)
#define FRACT_INC ((MICROSECONDS_PER_TIMER0_OVERFLOW % 1000) >> 3)
#define FRACT_MAX (1000 >> 3)

volatile unsigned long timer0_millis = 0;
volatile unsigned long timer0_fract = 0;
volatile unsigned long timer0_overflow_count = 0;

ISR(TIMER0_COMP_vect)
{
	unsigned long m = timer0_millis;
	unsigned char f = timer0_fract;
	
	m += MILLIS_INC;
	f += FRACT_INC;
	if (f >= FRACT_MAX) {
		f -= FRACT_MAX;
		m += 1;
	}
	
	timer0_fract = f;
	timer0_millis = m;
	timer0_overflow_count++;
}

void initSystime()
{
	TCCR0 |= (1<<CS01);
	TCCR0 |= (1<<WGM01);
	TIMSK |= (1<<OCIE0);
	TCNT0 = 0;
	
	OCR0 = 0x7F;
	
	sei();
}

unsigned long millis()
{
	unsigned long m;
	uint8_t oldSREG = SREG;
	
	// disable interrupts while we read timer0_millis or we might get an
	// inconsistent value (e.g. in the middle of a write to timer0_millis)
	cli();
	m = timer0_millis;
	SREG = oldSREG;
	
	return m;
}
