/*
 * sensors.h
 *
 * Created: 28.10.2014 8:18:16
 *  Author: Home
 */ 


#ifndef SENSORS_H_
#define SENSORS_H_

#include "button.h"

//typedef Button Sensor;

typedef char (*contactDetectorPtr)();

typedef enum CheckType
{
	Checked,
	Unchecked
} Check;

typedef enum ContactDetectType
{
	Detected,
	Undetected
} ContactDetect;

typedef struct SensorType
{
	// specify strong signal detected or not
	ContactDetect ContactDetected;
	
	// Specify current signal level
	Check IsChecked;
	
	// time when first detection occurred
	unsigned long checkTime;
	
	// time required to change state
	unsigned int setTimeDelta;
	
	// function to detect signal
	contactDetectorPtr IsContactDetected;
	
} Sensor;





#define LowLevelSensor_DDR DDRA
#define LowLevelSensor_Port PORTA
#define LowLevelSensor_Pin PINA3
#define LowLevelSensor_IN PINA

#define MediumLevelSensor_DDR DDRA
#define MediumelSensor_Port PORTA
#define MediumLevelSensor_Pin PINA4
#define MediumLevelSensor_IN PINA

#define FullLevelSensor_DDR DDRA
#define FullLevelSensor_Port PORTA
#define FullLevelSensor_Pin PINA5
#define FullLevelSensor_IN PINA

#define OverflowFirstLevelSensor_DDR DDRA
#define OverflowFirstLevelSensor_Port PORTA
#define OverflowFirstLevelSensor_Pin PINA6
#define OverflowFirstLevelSensor_IN PINA

#define OverflowSecondLevelSensor_DDR DDRA
#define OverflowSecondLevelSensor_Port PORTA
#define OverflowSecondLevelSensor_Pin PINA7
#define OverflowSecondLevelSensor_IN PINA


typedef enum WaterLevelType
{	
	Low,	
	Medium,	
	Full,	
	OverFull,
	Error
} WaterLevel;

//typedef struct SensorsType
//{
	//Sensor Low;
	//Sensor Medium;
	//Sensor Full;
	//Sensor OverflowFirst;
	//Sensor OverflowSecond;
//} Sensors;

void initSensors();
WaterLevel getWaterLevel();

void updateSensors(unsigned long currentTime);
void updateSensor(Sensor* sensor, unsigned long currentTime);
void initSensor(Sensor* sensor,  contactDetectorPtr contactDetector, unsigned int setTimeDelta);

#endif /* SENSORS_H_ */