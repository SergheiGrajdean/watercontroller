/*
 * WaterController.c
 *
 * Created: 27.10.2014 11:03:51
 *  Author: Serghei Grajdean
 */ 



#include "WaterController.h"
#include <avr/io.h>
#include <util/delay.h>
#include <avr/eeprom.h>

#include "button.h"
#include "sensors.h"
#include "Leds.h"
#include "systime.h"
#include "supape.h"

#define Is_Timer_Bit_Set ((PIND & 1<<PIND3) == (1<<PIND3))


typedef char ApplicationState;
#define Application_State_Initializing 0
#define Application_State_Programming 1
#define Application_State_Standby 2
#define Application_State_Filling 3 
#define Application_State_Error 21
#define Application_State_OverfullError 22

typedef struct ApplicationType
{
	ApplicationState applicationState;
	char timer;
	uint32_t time;
} Application;


void TurnOnSupape(void);
void TurnOffSupape(void);
void setProgrammingState();

void clickHandler();
void longClickHandler();

void updateWaterLevelLeds(WaterLevel waterLevel);

void setFillingState(Application* application);
void setStandByState(Application* application);

Application application = 
{
	.applicationState = Application_State_Programming,
	.timer = 0,
};

int main(void)
{
	initSystime();
	initLeds();	
	initSensors();
	setProgrammingState();
	initSupape();
	
	Button* timerButton = createButton();
	timerButton->clickHandler = &clickHandler;
	timerButton->longClickHandler = &longClickHandler;
		
	DDRD &= ~(1<<PIND3);
	
	application.applicationState = Application_State_Standby;
	
    while(1)
    {
		unsigned long currentTime = millis();
		
		//updateButton(Is_Timer_Bit_Set, timerButton);
		//
		//updateLed(&ProgrammingLed);
		//updateLed(&LowLevelLed);
		//updateLed(&MediumLevelLed);
		//updateLed(&FullLevelLed);
		updateSensors(currentTime);
		
		WaterLevel waterLevel = getWaterLevel();

		//updateWaterLevelLeds(waterLevel);
			
		switch(application.applicationState)
		{
			case Application_State_Standby :
			{
				break;
			}
			case Application_State_Filling :
			{
				if(waterLevel != Low && waterLevel != Medium)
				{
					setStandByState(&application);
				}
				break;	
			}
			//case Application_State_Error:
			default:
			{
				setBlinkLed(&ProgrammingLed);
				break;
			}
		}
    }
}



void clickHandler()
{
	if(application.applicationState == Application_State_Standby)
	{
		setFillingState(&application);
	}
	else
	{
		setStandByState(&application);
	}
	
}

void longClickHandler()
{
	clickHandler();
}

void setProgrammingState()
{
	application.applicationState = Application_State_Programming;
	ProgrammingLed.times = 0;
	ProgrammingLed.turnOff();
	
	LowLevelLed.times = -1;
	MediumLevelLed.times = -1;
	FullLevelLed.times = -1;
	
	LowLevelLed.turnOff();
	MediumLevelLed.turnOff();
	FullLevelLed.turnOff();
}

void setFillingState(Application* application)
{
	TurnOnSupape();
	turnOnLed(&ProgrammingLed);
	application->applicationState = Application_State_Filling;
}

void setStandByState(Application* application)
{
	TurnOffSupape();
	setBlinkLed(&ProgrammingLed);
	application->applicationState = Application_State_Standby;
}

void updateWaterLevelLeds(WaterLevel waterLevel)
{
	switch(waterLevel)
	{
		Low_Level_Led_Off();
		Medium_Level_Led_Off();
		Full_Level_Led_Off();
		
		case Low:
		{
			Low_Level_Led_On();
			Medium_Level_Led_Off();
			Full_Level_Led_Off();
			break;
		}
		case Medium:
		{
			Low_Level_Led_On();
			Medium_Level_Led_On();
			Full_Level_Led_Off();
			
			break;
		}
		case Full :
		{
			Low_Level_Led_On();
			Medium_Level_Led_On();
			Full_Level_Led_On();
			break;
		}
		case OverFull :
		{
			
		}
		default :
		{
			break;
		}
	}
}
