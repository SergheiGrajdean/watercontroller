/*
 * button.c
 *
 * Created: 27.10.2014 16:15:25
 *  Author: Serghei Grajdean
 */ 


#include "button.h"

void updateButton(bool isPressed, Button* buttonPtr)
{
	if(isPressed)
	{
		if(buttonPtr->keyState == KeyUp)
		{
			buttonPtr->pressTime = millis();
			buttonPtr->keyState = KeyDown;
		}
	}
	else if(buttonPtr->keyState == KeyDown)
	{
		unsigned long currentTime = millis();
		unsigned holdTime = currentTime - buttonPtr->pressTime;
		
		if(holdTime > 1000)
		{
			buttonPtr->longClickHandler();
		}
		else if(holdTime > 100)
		{
			buttonPtr->clickHandler();
		}
		
		buttonPtr->keyState = KeyUp;
	}
}

Button* createButton()
{
	Button* buttonPtr = malloc(sizeof(Button));
	buttonPtr->keyState = KeyUp;
	buttonPtr->pressTime = 0;
	
	return buttonPtr;
}

