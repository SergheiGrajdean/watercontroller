/*
 * Leds.h
 *
 * Created: 04.11.2014 22:28:19
 *  Author: Home
 */ 


#ifndef LEDS_H_
#define LEDS_H_

#define F_CPU 1000000

#include <avr/io.h>
#include <util/delay.h>

typedef void (*switchPtr)(void);

typedef struct ledType
{
	char times;
	unsigned long ChangeTime;
	char IsOn;
	switchPtr turnOn;
	switchPtr turnOff;
	
} Led;

Led LevelLedsState;
Led ProgrammingLed;

Led LowLevelLed;
Led MediumLevelLed;
Led FullLevelLed;

#define Short_Blink_Delay_Time 500 
#define Long_Blink_Delay_Time 2000

#define Led_On(Port,Pin) Port |= (1<<Pin)
#define Led_Off(Port,Pin) Port &= ~(1<<Pin)

// Programming_Button_Led
#define Programming_Button_Led_DDR DDRD
#define Programming_Button_Led_Port PORTD
#define Programming_Button_Led_Pin PIND6

#define Programming_Button_Led_Long_Blink() Programming_Button_Led_On(); _delay_ms(Long_Blink_Delay_Time);Programming_Button_Led_Off();

// Low water level led
#define Low_Level_Led_DDR DDRA
#define Low_Level_Led_Port PORTA
#define Low_Level_Led_Pin PINA0
void Low_Level_Led_On();
void Low_Level_Led_Off();

#define Medium_Level_Led_DDR DDRA
#define Medium_Level_Led_Port PORTA
#define Medium_Level_Led_Pin PINA1

void Medium_Level_Led_On();
void Medium_Level_Led_Off(); 

#define Full_Level_Led_DDR DDRA
#define Full_Level_Led_Port PORTA
#define Full_Level_Led_Pin PINA2
void Full_Level_Led_On();
void Full_Level_Led_Off();

void initLeds();
void blinkAllLeds();
void blinkLevelLeds();

void initLed(Led* led, switchPtr on, switchPtr off);

void Programming_Button_Led_On();
void Programming_Button_Led_Off();

void updateLed(Led* led);

void setBlinkLed(Led* led);
void turnOnLed(Led* led);
void turnOffLed(Led* led);

#endif /* LEDS_H_ */