/*
 * Leds.c
 *
 * Created: 19.04.2015 17:55:42
 *  Author: Home
 */ 

#include "Leds.h"
#include "systime.h"

void initLeds()
{
	initLed(&ProgrammingLed, &Programming_Button_Led_On, &Programming_Button_Led_Off);	
	initLed(&LowLevelLed, &Low_Level_Led_On, &Low_Level_Led_Off);
	initLed(&MediumLevelLed, &Medium_Level_Led_On, &Medium_Level_Led_Off);
	initLed(&FullLevelLed, &Full_Level_Led_On, &Full_Level_Led_Off);
	
	
	Programming_Button_Led_DDR |= (1<<Programming_Button_Led_Pin);
	
	Low_Level_Led_DDR |= (1<<Low_Level_Led_Pin);
	Medium_Level_Led_DDR |= (1<<Medium_Level_Led_Pin);
	Full_Level_Led_DDR |= (1<<Full_Level_Led_Pin);
	
	blinkAllLeds();
}

void initLed(Led* led, switchPtr on, switchPtr off)
{
	led->ChangeTime=0;
	led->IsOn = 0;
	led->times = 0;
	led->turnOn = on;
	led->turnOff = off;
}

void blinkAllLeds()
{
	ProgrammingLed.times = 2;
	LowLevelLed.times = 2;
	MediumLevelLed.times = 2;
	FullLevelLed.times = 2;
}

void _blink(Led* led)
{
	unsigned long time = millis();
	
	if(led->ChangeTime + 500 < time)
	{
		led->ChangeTime = time;
		
		if(led->times>0)
		{
			led->times--;
		}
		
		if(led->IsOn == 0)
		{
			led->turnOn();
			led->IsOn = 1;
		}
		else
		{
			led->turnOff();
			led->IsOn = 0;
		}
	}
}

void updateLed(Led* led)
{
	if(led->times != 0)
	{
		_blink(led);
	}
}

void Programming_Button_Led_On()
{
	Led_On(Programming_Button_Led_Port, Programming_Button_Led_Pin);
}

void Programming_Button_Led_Off()
{
	Led_Off(Programming_Button_Led_Port, Programming_Button_Led_Pin);
}

void Low_Level_Led_On() 
{
	Led_On(Low_Level_Led_Port, Low_Level_Led_Pin);
}

void Low_Level_Led_Off()
{	 
	Led_Off(Low_Level_Led_Port, Low_Level_Led_Pin);
}
	
void Medium_Level_Led_On() 
{
	Led_On(Medium_Level_Led_Port, Medium_Level_Led_Pin);
}

void Medium_Level_Led_Off() 
{
	Led_Off(Medium_Level_Led_Port, Medium_Level_Led_Pin);
}

void Full_Level_Led_On()
{ 
	Led_On(Full_Level_Led_Port, Full_Level_Led_Pin);
}

void Full_Level_Led_Off() 
{
	Led_Off(Full_Level_Led_Port, Full_Level_Led_Pin);
}

void setBlinkLed(Led* led)
{
	led->times = -1;
}

void turnOnLed(Led* led)
{
	led->times = 0;
	led->turnOn();
}

void turnOffLed(Led* led)
{
	led->times = 0;
	led->turnOff();
}
